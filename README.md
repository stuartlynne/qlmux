# qlmux

QL Mux provides a fast replacement for cups lp to send labels to Brother QL label printers, allowing for  round-robin and backup from pools of printers.